package com.touchsi.sutee.helloandroid;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.touchsi.sutee.helloandroid.Number.OnNumberChangeListener;

public class MainActivity extends Activity implements OnClickListener,
		OnNumberChangeListener {

	private TextView txtNumber;
	private Button btnTwo;
	private Button btnFour;
	private Number mNumber = new Number();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mNumber.setOnNumberChangeListener(this);

		txtNumber = (TextView) findViewById(R.id.txtNumber);
		btnTwo = (Button) findViewById(R.id.btnTwo);
		btnFour = (Button) findViewById(R.id.btnFour);

		btnTwo.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mNumber.setValue(2);
			}
		});

		btnFour.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	public void one(View view) {
		mNumber.setValue(1);
	}

	public void three(View view) {
		mNumber.setValue(3);
	}

	public void onClick(View v) {
		mNumber.setValue(4);
	}

	public void onNumberChange(Number number) {
		txtNumber.setText(String.valueOf(number.getValue()));
	}

}
