package com.touchsi.sutee.dotdot;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
		TextView txtCoordX = (TextView) findViewById(R.id.txtCoordX);
		TextView txtCoordY = (TextView) findViewById(R.id.txtCoordY);
		Intent intent = getIntent();
		if (intent != null && intent.getExtras() != null) {
			int coordX = intent.getExtras().getInt(Contants.COORD_X_KEY);
			int coordY = intent.getExtras().getInt(Contants.COORD_Y_KEY);
			txtCoordX.setText(String.valueOf(coordX));
			txtCoordY.setText(String.valueOf(coordY));
		}
		Intent intentResult = new Intent();
		intentResult.putExtra("Result", "Hello");
		setResult(0, intentResult);
	}
	
	

}
