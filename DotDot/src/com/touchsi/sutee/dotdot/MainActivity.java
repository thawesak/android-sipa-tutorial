package com.touchsi.sutee.dotdot;

import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;

import com.touchsi.sutee.dotdot.Dots.OnDotsChangeListener;

public class MainActivity extends Activity implements OnDotsChangeListener,
		OnItemClickListener {

	private static final int MAX_COORD_Y = 300;
	private static final int MAX_COORD_X = 300;
	private static final int MENU_CLEAR_ITEM = 1001;
	private static final int MENU_DELETE_ITEM = 1002;
	private static final int MENU_EDIT_ITEM = 1003;
	private Dots mDots = new Dots();
	private Random mGenerator = new Random();
	private ListView mListView;
	private DotListAdapter mAdapter;

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_CLEAR_ITEM:
			mDots.clear();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(Menu.NONE, MENU_CLEAR_ITEM, Menu.NONE,
				getString(R.string.clear));
		return true;
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		switch (item.getItemId()) {
		case MENU_DELETE_ITEM:
			confirmDelete(info.position);
			return true;
		case MENU_EDIT_ITEM:
			editDot(info.position);
		}
		return super.onContextItemSelected(item);
	}

	private void editDot(final int position) {
		View view = getLayoutInflater().inflate(R.layout.edit_dialog, null);
		final EditText edtCoordX = (EditText) view.findViewById(R.id.edtCoordX);
		final EditText edtCoordY = (EditText) view.findViewById(R.id.edtCoordY);
		Dot dot = mDots.get(position);
		edtCoordX.setText(String.valueOf(dot.getCoordX()));
		edtCoordY.setText(String.valueOf(dot.getCoordY()));
		new AlertDialog.Builder(this).setTitle("Edit").setView(view)
				.setPositiveButton(R.string.yes, new OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						int coordX = Integer.parseInt(edtCoordX.getText()
								.toString());
						int coordY = Integer.parseInt(edtCoordY.getText()
								.toString());
						mDots.edit(position, coordX, coordY);
					}
				}).setNegativeButton(R.string.no, null).show();
	}

	private void confirmDelete(final int position) {
		new AlertDialog.Builder(this).setTitle("Confirm Delete")
				.setMessage("Are you sure?")
				.setPositiveButton(R.string.yes, new OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						mDots.delete(position);
					}
				}).setNegativeButton(R.string.no, null).show();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		menu.add(Menu.NONE, MENU_DELETE_ITEM, Menu.NONE, R.string.delete);
		menu.add(Menu.NONE, MENU_EDIT_ITEM, Menu.NONE, R.string.edit);
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mAdapter = new DotListAdapter(this) {
			public Object getItem(int position) {
				return mDots.get(position);
			}

			public int getCount() {
				return mDots.size();
			}
		};

		mListView = (ListView) findViewById(R.id.listView);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(this);

		mDots.setOnDotsChangeListener(this);
		registerForContextMenu(mListView);
	}

	public void randomDot(View view) {
		int coordX = mGenerator.nextInt(MAX_COORD_X);
		int coordY = mGenerator.nextInt(MAX_COORD_Y);

		Dot dot = new Dot(coordX, coordY);
		mDots.insert(dot);
	}

	public void onDotsChange(Dots dots) {
		mAdapter.notifyDataSetChanged();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
		Dot dot = mDots.get(position);
		Intent intent = new Intent(this, SecondActivity.class);
		intent.putExtra(Contants.COORD_X_KEY, dot.getCoordX());
		intent.putExtra(Contants.COORD_Y_KEY, dot.getCoordY());
		startActivityForResult(intent, 0);
	}
}
